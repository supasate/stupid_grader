<?php

class ApiControllerTest extends CTestCase
{
	
	public function setUp() {
		$this->api = new ApiControllerT(rand());
	}

	public function testFormatResponseHeader() {
		$this->assertEquals('HTTP/1.1 400 Bad Request', $this->api->formatResponseHeader('400'));
	}

	public function testCodeNotExist() {
		$this->assertEquals('HTTP/1.1 400 Bad Request', $this->api->formatResponseHeader('-1'));
	}

	public function testGetHello() {
		$this->assertEquals('Hello World', $this->api->getHello('World'));
	}

	public function tearDown() {
		unset($this->api);
	}
}