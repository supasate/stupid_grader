<?php

class GraderTest extends CTestCase
{

	public function setUp() {
		$this->grader = new Grader();
	}

	public function testGetAPlus() {
		$this->assertEquals('A+', $this->grader->getGrade(95));
		$this->assertEquals('A+', $this->grader->getGrade(100));
	}

	public function testGetA() {
		$this->assertEquals('A', $this->grader->getGrade(85));
		$this->assertEquals('A', $this->grader->getGrade(94));
	}

	public function testGetAMinus() {
		$this->assertEquals('A-', $this->grader->getGrade(80));
		$this->assertEquals('A-', $this->grader->getGrade(84));
	}

	public function testGetB() {
		$this->assertEquals('B', $this->grader->getGrade(70));
		$this->assertEquals('B', $this->grader->getGrade(79));
	}

	public function testGetC() {
		$this->assertEquals('C', $this->grader->getGrade(65));
		$this->assertEquals('C', $this->grader->getGrade(69));
	}

	public function testGetDPlus() {
		$this->assertEquals('D+', $this->grader->getGrade(60));
		$this->assertEquals('D+', $this->grader->getGrade(64));
	}

	public function testGetD() {
		$this->assertEquals('D', $this->grader->getGrade(55));
		$this->assertEquals('D', $this->grader->getGrade(59));
	}

	public function testGetDMinus() {
		$this->assertEquals('D-', $this->grader->getGrade(50));
		$this->assertEquals('D-', $this->grader->getGrade(54));
	}

	public function testGetFPlus() {
		$this->assertEquals('F+', $this->grader->getGrade(40));
		$this->assertEquals('F+', $this->grader->getGrade(49));
	}

	public function testGetF() {
		$this->assertEquals('F', $this->grader->getGrade(0));
		$this->assertEquals('F', $this->grader->getGrade(39));
	}

	public function testOutOfRange() {
		$this->assertEquals('Out of Range', $this->grader->getGrade(101));
		$this->assertEquals('Out of Range', $this->grader->getGrade(-1));
	}

	public function tearDown() {
		unset($this->grader);
	}
}
