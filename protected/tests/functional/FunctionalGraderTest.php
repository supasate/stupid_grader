<?php

class FunctaionalGraderTest extends WebTestCase
{
	public function testInput100ShouldGetGradeAPlus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '100');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('A+', $this->getAlert());
	}

	public function testInput95ShouldGetGradeAPlus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '95');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('A+', $this->getAlert());
	}

	public function testInput94ShouldGetGradeA()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '94');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('A', $this->getAlert());
	}

	public function testInput85ShouldGetGradeA()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '85');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('A', $this->getAlert());
	}	

	public function testInput84ShouldGetGradeAMinus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '84');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('A-', $this->getAlert());
	}

	public function testInput80ShouldGetGradeAMinus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '80');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('A-', $this->getAlert());
	}

	public function testInput79ShouldGetGradeB()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '79');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('B', $this->getAlert());
	}

	public function testInput70ShouldGetGradeB()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '70');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('B', $this->getAlert());
	}

	public function testInput69ShouldGetGradeC()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '69');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('C', $this->getAlert());
	}

	public function testInput65ShouldGetGradeC()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '65');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('C', $this->getAlert());
	}

	public function testInput64ShouldGetGradeDPlus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '64');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('D+', $this->getAlert());
	}

	public function testInput60ShouldGetGradeDPlus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '60');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('D+', $this->getAlert());
	}

	public function testInput59ShouldGetGradeD()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '59');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('D', $this->getAlert());
	}

	public function testInput55ShouldGetGradeD()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '55');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('D', $this->getAlert());
	}

	public function testInput54ShouldGetGradeDMinus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '54');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('D-', $this->getAlert());
	}

	public function testInput50ShouldGetGradeDMinus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '50');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('D-', $this->getAlert());
	}

	public function testInput49ShouldGetGradeFPlus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '49');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('F+', $this->getAlert());
	}

	public function testInput40ShouldGetGradeFPlus()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '40');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('F+', $this->getAlert());
	}

	public function testInput39ShouldGetGradeF()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '39');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('F', $this->getAlert());
	}

	public function testInput0ShouldGetGradeF()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '0');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('F', $this->getAlert());
	}

	public function testInput101ShouldGetOutOfRange()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '101');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('Out of Range', $this->getAlert());
	}

	public function testInputMinus1ShouldGetOutOfRange()
	{
		$this->open('?r=site/index');
		$this->assertElementPresent('name=score');
		$this->type('name=score', '-1');
		$this->click("//input[@value='Calculate']");
		$this->assertEquals('Out of Range', $this->getAlert());
	}
}
