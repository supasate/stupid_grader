<?php
class Grader {
	const OUT_OF_RANGE = 'Out of Range';

	public function getGrade($score) {
		$grade = self::OUT_OF_RANGE;

		if ($score >= 101) {
			$grade = self::OUT_OF_RANGE;
		}
		else if ($score >= 95) {
			$grade = 'A+';
		}	
		else if ($score >= 85) {
			$grade = 'A';
		}
		else if ($score >= 80) {
			$grade = 'A-';
		}
		else if ($score >= 70) {
			$grade = 'B';
		}
		else if ($score >= 65) {
			$grade = 'C';
		}
		else if ($score >= 60) {
			$grade = 'D+';
		}
		else if ($score >= 55) {
			$grade = 'D';
		}
		else if ($score >= 50) {
			$grade = 'D-';
		}
		else if ($score >= 40) {
			$grade = 'F+';
		}
		else if ($score >= 0) {
			$grade = 'F';
		}
		else {
			$grade = self::OUT_OF_RANGE;
		}
		return $grade;
	}
}
