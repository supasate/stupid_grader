<?php

class ApiController extends Controller {

	private $response_code = array('400' => 'Bad Request');

	public function formatResponseHeader($code) {

		if (!array_key_exists($code, $this->response_code)) {
			$code = '400';
		}

		return 'HTTP/1.1 ' . $code . ' ' . $this->response_code[$code];
	}

	public function getHello($text) {
		return 'Hello ' . $text;
	}
}
